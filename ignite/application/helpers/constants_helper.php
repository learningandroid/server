<?php

define('cons_null', 0);
define('cons_student', 1);
define('cons_faculty', 2);
define('cons_warden', 3);
define('cons_staff', 4);
define('error_none', 0);
define('error_username', 1);
define('normal', 0);
define('special', 1);
define('unresolved', 0);
define('resolved', 1);
define('zero', 0);
define('novote', 0);
define('upvote', 1);
define('downvote', 2);

date_default_timezone_set('Asia/Calcutta');

?>