<?php

function findUser($where, $context){

	$context->load->model('Model_students');
	$student = $context->Model_students->get_by($where); 
	if (isset($student['id'])) {	
		$user['error'] = error_none;
		$user['type'] = cons_student;
		$user['info'] = $student;
		return $user;
	}

	$context->load->model('Model_faculty');
	$faculty = $context->Model_faculty->get_by($where); 
	if (isset($faculty['id'])) {	
		$user['error'] = error_none;
		$user['type'] = cons_faculty;
		$user['info'] = $faculty;
		return $user;
	}

	$context->load->model('Model_warden');
	$warden = $context->Model_warden->get_by($where); 
	if (isset($warden['id'])) {	
		$user['error'] = error_none;
		$user['type'] = cons_warden;
		$user['info'] = $warden;
		return $user;
	}

	$context->load->model('Model_staff');
	$staff = $context->Model_staff->get_by($where); 
	if (isset($staff['id'])) {	
		$user['error'] = error_none;
		$user['type'] = cons_staff;
		$user['info'] = $staff;
		return $user;
	}

	$user['error'] = error_username;
	return $user;
}

function deleteUser($UserId, $context){

	$context->load->model('Model_students');
	$student = $context->Model_students->get_by(array('UserID'=> $UserId)); 
	if (isset($student['id'])) {	
		if ($student['Special'] == normal) {
			$context->Model_students->delete($student['id']);
			return true;
		}else{
			return false;
		}
	}

	$context->load->model('Model_faculty');
	$faculty = $context->Model_faculty->get_by(array('UserID'=> $UserId)); 
	if (isset($faculty['id'])) {	
		if ($faculty['Special'] == normal) {
			$context->Model_faculty->delete($faculty['id']);
			return true;
		}else{
			return false;
		} 
	}

	$context->load->model('Model_warden');
	$warden = $context->Model_warden->get_by(array('UserID'=> $UserId)); 
	if (isset($warden['id'])) {	
		if ($warden['Special'] == normal) {
			$context->Model_warden->delete($warden['id']);
			return true;
		}else{
			return false;
		} 
	}

	$context->load->model('Model_staff');
	$staff = $context->Model_staff->get_by(array('UserID'=> $UserId)); 
	if (isset($staff['id'])) {	
		if ($staff['Special'] == normal) {
			$context->Model_staff->delete($staff['id']);
			return true;
		}else{
			return false;
		} 
	}

	return false;
}

?>