<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_staff extends MY_Model{
	protected $_table = 'staff';
	protected $primary_key = 'id';
	protected $return_type = 'array';
}

?>